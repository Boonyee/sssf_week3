import Category from "../models/Category.js";

export default {
  Species: {
    category(parent) {
      return Category.findById(parent.category);
    },
  },

  Query: {
    categories: (parent, args) => {
      return Category.find();
    },
    category: (parent, args) => {
      return Category.findById(args.id);
    },
  },

  Mutation: {
    addCategory: (parent, args) => {
      const newCategory = new Category(args);

      return newCategory.save();
    },
  },
};
