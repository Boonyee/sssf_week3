import Species from "../models/Species.js";

export default {
  Animal: {
    species(parent) {
      return Species.findById(parent.species);
    },
  },

  Query: {
    species: (parent, args) => {
      return Species.find(args);
    },
  },

  Mutation: {
    addSpecies: (parent, args) => {
      const newSpecies = new Species(args);
      return newSpecies.save();
    },
  },
};
