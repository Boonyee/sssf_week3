import Animal from "../models/Animal.js";

export default {
  Query: {
    animals: (parent, args) => {
      return Animal.find();
    },
    animal: (parent, args) => {
      return Animal.findById(args.id);
    },
  },
  Mutation: {
    addAnimal: (parent, args) => {
      const newAnimal = new Animal(args);

      return newAnimal.save();
    },
    modifyAnimal: (parent, args) => {
      return Animal.findByIdAndUpdate(args.id, args);
    },
  },
};
