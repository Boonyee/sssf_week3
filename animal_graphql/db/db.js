import dotenv from "dotenv";
dotenv.config();
import mongoose from "mongoose";

(async () => {
  try {
    await mongoose.connect(process.env.LOCAL_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("DB connected successfully");
  } catch (err) {
    console.error("Connection to db failed", err);
  }
})();

export default mongoose.connection;
