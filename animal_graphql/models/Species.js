import mongoose from "mongoose";

const Schema = mongoose.Schema;

const speciesSchema = Schema({
  speciesName: String,
  category: {
    type: Schema.Types.ObjectId,
    ref: "category",
  },
});

export default mongoose.model("species", speciesSchema);
