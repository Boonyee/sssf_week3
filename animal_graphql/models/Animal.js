import mongoose from "mongoose";

const Schema = mongoose.Schema;

const animalSchema = Schema({
  animalName: String,
  species: {
    type: Schema.Types.ObjectId,
    ref: "species",
  },
});

export default mongoose.model("animals", animalSchema);
