import mongoose from "mongoose";

const Schema = mongoose.Schema;

const categorySchema = Schema({
  categoryName: String,
});

export default mongoose.model("category", categorySchema);
