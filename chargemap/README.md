# All Endpoints Examples

## 1. /auth/login

## POST

**Body: JSON**

```
{
    username:"yourUsername",
    password:"yourPassword"
}
```

response:

```
{
    "user": {
        "_id": "602ab39e4becd1315c5f8019",
        "username": "ilkkamtk",
        "full_name": "ilkka",
        "__v": 0
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDJhYjM5ZTRiZWNkMTMxNWM1ZjgwMTkiLCJ1c2VybmFtZSI6Imlsa2thbXRrIiwicGFzc3dvcmQiOiIkMmIkMTIkcU1UNkZDMmRENkZhRzE3dEovT2lmT2taMzNNOU11RmdGUHF0b2dvMXZkVnJjR3RTNko2QzIiLCJmdWxsX25hbWUiOiJpbGtrYSIsIl9fdiI6MCwiaWF0IjoxNjE2ODUzMzI0fQ.rQU1J8991JJW7mKxpOiQvW-Gw2HX0DOf3S9Z3FDk4qc"
}
```

Put the token in "Bearer Token" of Authentication in where it needs.

## 2. /station

## GET

- request all stations with default limit: 10

> /station?limit=10

Parameters:

limit : limit number of results (dafault : 10), optional.

- request station by ID

> /station/{ID}

- request stations within certain rectangle area (GeoJSON)

> /station?topRight={"lat":60.2821946,"lng":25.036108}&bottomLeft={"lat":60.1552076,"lng":24.7816538}

Parameters:

NOTE: When doing GeoJSON request, you must fill both topRight and bottomLeft parameters

1. topRight: {"lat": numbers, "lng": numbers}
2. bottomLeft: {"lat": numbers, "lng": numbers}

## POST

- create a new station (authentication needed).

> /station

**Body: JSON**

```
{
    "Station": {
        "Title": "Capgemini Oy",
        "Town": "Espoo",
        "AddressLine1": "Sinimäentie 8b",
        "StateOrProvince": "Southern Finland",
        "Postcode": "02630",
        "Location": {
        "coordinates": [24.77772323548868, 60.203353130088146]
        }
    },
    "Connections":[
        {
        "ConnectionTypeID": "5e39eecac5598269fdad81a0",
        "CurrentTypeID": "5e39ef4a6921476aaf62404a",
        "LevelID": "5e39edf7bb7ae768f05cf2bc",
        "Quantity": 2
        }
    ]
}
```

# PUT

- modify an existing station (authentication needed).

> /station

**Body: JSON**

```
{
"Station": {
    "_id": "5e8df9a81f87eb168e4c6757",
    "Title": "Testi",
    "Town": "Espoo",
    "AddressLine1": "Sinimäentie 8b",
    "StateOrProvince": "Southern Finland",
    "Postcode": "02630",
    "Location": {
        "coordinates": [24.77772323548868, 60.203353130088146]
        }
},
"Connections":[
        {
        "_id": "5e8df9a81f87eb168e4c6756",
        "ConnectionTypeID": "5e39eecac5598269fdad81a0",
        "CurrentTypeID": "5e39ef4a6921476aaf62404a",
        "LevelID": "5e39edf7bb7ae768f05cf2bc",
        "Quantity": 7
        }
  ]
}
```

# DELETE

- delete a station by ID

> /station/{ID}
