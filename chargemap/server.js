"use strict";
require("dotenv").config();
const express = require("express");
const app = express();
const port = 3000;
const stationRoute = require("./routes/stationRoute.js");
const authRoute = require("./routes/authRoute.js");
const db = require("./utils/db.js");
const schemas = require("./schemas/index.js");
const resolvers = require("./resolvers/index.js");
const { ApolloServer } = require("apollo-server-express");

// const passport = require("./utils/pass");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/station", stationRoute);
// app.use("/auth", authRoute);

db.on("connected", () => {
  try {
    const server = new ApolloServer({
      typeDefs: schemas,
      resolvers,
    });

    server.applyMiddleware({ app });

    app.listen({ port: port }, () =>
      console.log(
        `🚀 Server ready at http://localhost:3000${server.graphqlPath}`
      )
    );
  } catch (e) {
    throw e;
  }
});
