"use strict";

const Station = require("../models/Station");
const Connection = require("../models/Connection");
const { Types } = require("mongoose");

const rectangleBounds = (topRight, bottomLeft) => {
  //   console.log("topright", topRight);
  //   console.log("bottomLeft", bottomLeft);
  return {
    type: "Polygon",
    coordinates: [
      [
        [bottomLeft.lng, bottomLeft.lat],
        [bottomLeft.lng, topRight.lat],
        [topRight.lng, topRight.lat],
        [topRight.lng, bottomLeft.lat],
        [bottomLeft.lng, bottomLeft.lat],
      ],
    ],
  };
};

const getAllStation = async (req, res) => {
  try {
    const limit = Number(req.query.limit) || 10;
    if (req.query.topRight && req.query.bottomLeft) {
      const docs = await Station.find()
        .where("Location")
        .within(
          rectangleBounds(
            JSON.parse(req.query.topRight),
            JSON.parse(req.query.bottomLeft)
          )
        )
        .limit(limit)
        .populate("Connections");
      res.send(docs);
    } else {
      const docs = await Station.find({}).limit(limit).populate("Connections");
      res.send(docs);
    }
  } catch (err) {
    throw err;
  }
};

const getStationById = async (req, res) => {
  try {
    const doc = await Station.findById(Types.ObjectId(req.params.id)).populate(
      "Connections"
    );
    res.send(doc);
  } catch (error) {
    throw err;
  }
};

const createStation = async (req, res) => {
  try {
    const doc = await Station.create({
      ...req.body.Station,
      Connections: await Promise.all(
        req.body.Connections.map(async (element) => {
          return await Connection.create({
            ...element,
          })._id;
        })
      ),
    });
    res.send(doc);
  } catch (err) {
    throw err;
  }
};

const modifyStation = async (req, res) => {
  try {
    const doc = await Station.findByIdAndUpdate(
      Types.ObjectId(req.body.Station._id),
      {
        ...req.body.Station,
        Connections: await Promise.all(
          req.body.Connections.map(async (element) => {
            return await Connection.findByIdAndUpdate(
              Types.ObjectId(element._id),
              {
                ...element,
              }
            )._id;
          })
        ),
      },
      { new: true }
    );
    console.log(doc);
    res.send(doc);
  } catch (err) {
    res.status(400).send("an error occured.");
    throw err;
  }
};

const deleteStation = async (req, res) => {
  try {
    const doc = await Station.findByIdAndDelete(Types.ObjectId(req.params.id));
    res.send(doc);
  } catch (error) {
    res.status(400).send("an error occured.");
    throw err;
  }
};

module.exports = {
  getAllStation,
  getStationById,
  createStation,
  modifyStation,
  deleteStation,
};
