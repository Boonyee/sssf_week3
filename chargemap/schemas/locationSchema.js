const { gql } = require("apollo-server-express");

module.exports = gql`
  type Location {
    coordinates: [Float!]!
    type: Type!
  }

  enum Type {
    Point
  }
`;
