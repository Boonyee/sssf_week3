const connectionSchema = require("./connectionSchema.js");
const connectionTypeSchema = require("./connectionTypeSchema.js");
const currentTypeSchema = require("./currentTypeSchema.js");
const levelSchema = require("./levelSchema.js");
const locationSchema = require("./locationSchema.js");
const stationSchema = require("./stationSchema.js");

const { gql } = require("apollo-server-express");

const linkSchema = gql`
  type Query {
    _: Boolean
  }
  type Mutation {
    _: Boolean
  }
`;

module.exports = [
  linkSchema,
  connectionSchema,
  connectionTypeSchema,
  levelSchema,
  currentTypeSchema,
  locationSchema,
  stationSchema,
];
