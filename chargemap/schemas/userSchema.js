const { gql } = require("apollo-server-express");

module.exports = gql`
  type User {
    id: ID
    username: String
    password: String
    full_name: String
  }
`;
