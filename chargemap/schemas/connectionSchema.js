const { gql } = require("apollo-server-express");

module.exports = gql`
  type Connection {
    id: ID!
    ConnectionTypeID: ConnectionType
    LevelID: Level
    CurrentTypeID: CurrentType
    Quantity: Int
  }
`;
