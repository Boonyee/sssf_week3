//module is in strict mode by default ;)
const mongoose = require("mongoose");

(async () => {
  try {
    await mongoose.connect(process.env.LOCAL_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log("DB connected successfully");
  } catch (err) {
    console.error("Connection to db failed", err);
  }
})();

module.exports = mongoose.connection;
