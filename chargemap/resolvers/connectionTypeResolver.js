const ConnectionType = require("../models/ConnectionType.js");

module.exports = {
  Connection: {
    ConnectionTypeID(parent) {
      return ConnectionType.findById(parent.ConnectionTypeID);
    },
  },

  Query: {
    connectiontypes: () => {
      return ConnectionType.find();
    },
  },
  //   Mutation: {
  //     addSpecies: (parent, args) => {
  //       const newSpecies = new Species(args);
  //       return newSpecies.save();
  //     },
  //   },
};
