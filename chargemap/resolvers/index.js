const levelResolver = require("./levelResolver.js");
const currentTypeResolver = require("./currentTypeResolver.js");
const connectionTypeResolver = require("./connectionTypeResolver.js");
const stationResolver = require("./stationResolver");
const connectionResolver = require("./connectionResolver");

module.exports = [
  levelResolver,
  currentTypeResolver,
  connectionTypeResolver,
  stationResolver,
  connectionResolver,
];
