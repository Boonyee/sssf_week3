const Station = require("../models/Station.js");
const Connection = require("../models/Connection.js");

const rectangleBounds = (bottomLeft, topRight) => {
  //   console.log("topright", topRight);
  //   console.log("bottomLeft", bottomLeft);
  return {
    type: "Polygon",
    coordinates: [
      [
        [bottomLeft.lng, bottomLeft.lat],
        [bottomLeft.lng, topRight.lat],
        [topRight.lng, topRight.lat],
        [topRight.lng, bottomLeft.lat],
        [bottomLeft.lng, bottomLeft.lat],
      ],
    ],
  };
};

module.exports = {
  Query: {
    stations: (parent, { start, limit, bounds }) => {
      return bounds
        ? Station.find()
            .skip(start)
            .limit(limit)
            .where("Location")
            .within(rectangleBounds(bounds._southWest, bounds._northEast))
        : Station.find().skip(start).limit(limit);
    },
    station: (parent, { id }) => {
      return Station.findById(id);
    },
  },

  Mutation: {
    addStation: async (parent, args) =>
      await Station.create({
        ...args,
        Location: {
          type: "Point",
          coordinates: args.Location.coordinates,
        },
        Connections: await Promise.all(
          args.Connections.map((element) =>
            Connection.create({ ...element, id: undefined })
          )
        ),
      }),

    deleteStation: async (parent, { id }) =>
      await Station.findByIdAndDelete(id),

    modifyStation: async (parent, args) => {
      return await Station.findByIdAndUpdate(
        args.id,
        {
          ...args,
          Connections: await Promise.all(
            args.Connections.map(async (element) => {
              return await Connection.findByIdAndUpdate(element.id, {
                ...element,
              });
            })
          ),
        },
        { new: true }
      );
    },
  },
};
