const CurrentType = require("../models/CurrentType.js");

module.exports = {
  Query: {
    currenttypes: () => {
      return CurrentType.find();
    },
  },
  Connection: {
    CurrentTypeID(parent) {
      return CurrentType.findById(parent.CurrentTypeID);
    },
  },
};
